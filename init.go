package main

import (
    "flag"
    "log"
    "os"
    "strconv"
)

var (
    argFilename     string
    argX            int
    argY            int
    optSetBlocked   string
    optSetSurface   string
    optSetElevation string
    optHelp         bool
    optVerbose      bool
)

// Initializes the program and parses command-line arguments and options.
// Called before the main function to separate argument parsing from core
// application logic.
func init() {
    flag.BoolVar(&optHelp, "help", false, "Request help documentation")
    flag.BoolVar(&optHelp, "h", optHelp, "Request help documentation")
    flag.BoolVar(&optHelp, "?", optHelp, "Request help documentation")
    flag.BoolVar(&optVerbose, "verbose", false, "Enables verbose logging")
    flag.BoolVar(&optVerbose, "v", optVerbose, "Enables verbose logging")
    flag.StringVar(&optSetBlocked, "block", "", "Set blocked")
    flag.StringVar(&optSetBlocked, "b", optSetBlocked, "Set blocked")
    flag.StringVar(&optSetSurface, "surface", "", "Set surface type")
    flag.StringVar(&optSetSurface, "s", optSetSurface, "Set surface type")
    flag.StringVar(&optSetElevation, "elevation", "", "Set elevation")
    flag.StringVar(&optSetElevation, "e", optSetElevation, "Set elevation")
    flag.Usage = help
    flag.Parse()

    if !flag.Parsed() || optHelp || len(flag.Args()) < 3 {
        flag.Usage()
    }

    var err error
    argFilename = flag.Arg(0)
    if argX, err = strconv.Atoi(flag.Arg(1)); err != nil { panic(err) }
    if argY, err = strconv.Atoi(flag.Arg(2)); err != nil { panic(err) }

    if !optVerbose {
        devnull, _ := os.Open(os.DevNull)
        log.SetOutput(devnull)
    }
}
