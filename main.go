package main

import (
    "encoding/binary"
    "fmt"
    "io/ioutil"
    "log"
    "os"
    "strconv"
)

// Floor is a program for modifying Conquer Online data maps (DMaps) with
// custom cell definitions. Each cell of a map contains a coordinate's block
// toggle, surface feedback type, and elevation. The new or current values of
// the cell will be outputed after each command.
func main() {    
    // Open file and prepare buffer
    file, err := os.Open(argFilename)
    if err != nil { panic(err) }
    defer file.Close()
    
    fileinfo, err := file.Stat()
    if err != nil { panic(err) }
    size := fileinfo.Size()
    
    buf := make([]byte, size)
    _, err = file.Read(buf)
    if err != nil { panic(err) }
    
    // Read map information to seek to cell
    width := binary.LittleEndian.Uint32(buf[268:])
    height := binary.LittleEndian.Uint32(buf[272:])
    offset := 276 + int64(uint32(argY) * ((width * 6) + 4)) + int64(argX * 6)
    if int(height) < argY { panic(err) }
    if int(width) < argX { panic(err) }
    log.Println("main.go: floor height =", height)
    log.Println("main.go: floor width =", width)
    log.Println("main.go: floor offset =", offset)

    // Read in original cell values
    blocked := binary.LittleEndian.Uint16(buf[offset:])
    surface := binary.LittleEndian.Uint16(buf[offset + 2:])
    elevation := int16(binary.LittleEndian.Uint16(buf[offset + 4:]))
    log.Println("main.go: cell blocked =", blocked)
    log.Println("main.go: cell surface =", surface)
    log.Println("main.go: cell elevation =", elevation)

    // Write out block change if it was specified
    write := false
    if optSetBlocked != "" {
        var parsedSetBlocked int
        parsedSetBlocked, err = strconv.Atoi(optSetBlocked)
        if err != nil { panic(err) }

        blocked = uint16(parsedSetBlocked)
        binary.LittleEndian.PutUint16(buf[offset:], blocked)
        log.Println("main.go: wrote blocked =", blocked)
        write = true
    }

    // Write out surface change if it was specified
    if optSetSurface != "" {
        var parsedSetSurface int
        parsedSetSurface, err = strconv.Atoi(optSetSurface)
        if err != nil { panic(err) }
        
        surface = uint16(parsedSetSurface)
        binary.LittleEndian.PutUint16(buf[offset + 2:], surface)
        log.Println("main.go: wrote surface =", surface)
        write = true
    }

    // Write out surface change if it was specified
    if optSetElevation != "" {
        var parsedSetElevation int
        parsedSetElevation, err = strconv.Atoi(optSetElevation)
        if err != nil { panic(err) }
        
        elevation = int16(parsedSetElevation)
        binary.LittleEndian.PutUint16(buf[offset + 2:], uint16(elevation))
        log.Println("main.go: wrote elevation =", elevation)
        write = true
    }

    // Check if a write occurred and write out the buffer
    if write {
        // Calculate the checksum
        var checksum uint32
        start := 276 + int(uint32(argY) * ((width * 6) + 4))
        end := start + int(width * 6)
        for i, x := start, 0; i < end; i += 6 {
            checksum += 
                (uint32(binary.LittleEndian.Uint16(buf[i:]))) *
                (uint32(binary.LittleEndian.Uint16(buf[i + 2:])) + uint32(argY) + 1) +
                (uint32(binary.LittleEndian.Uint16(buf[i + 4:])) + 2) *
                (uint32(binary.LittleEndian.Uint16(buf[i + 2:])) + uint32(x) + 1)
            x++
        }

        // Correct the checksum and write to file
        log.Println("main.go: old checksum =", binary.LittleEndian.Uint32(buf[end:]))
        binary.LittleEndian.PutUint32(buf[end:], checksum)
        log.Println("main.go: new checksum =", checksum)
        ioutil.WriteFile(file.Name(), buf, 0)
    }

    // Print values
    fmt.Println("cell blocked =", blocked)
    fmt.Println("cell surface =", surface)
    fmt.Println("cell elevation =", elevation)
}
